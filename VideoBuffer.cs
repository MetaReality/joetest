﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;
using System.IO;
using UnityEditor;


public class VideoBuffer : MonoBehaviour {

	WebCamTexture wCT;
	WebCamHelper wbHelper;

	int NumberOfFrames = 10;
	int accumThreshold = 25;

	float accumFrameWeight = 0;
	float currentFrameWeight = 0;

	int colorArraySize = 0;
	Texture2D frameTemp;
	Mat TempPix;

	Mat currentMat,accumBuffer;
	Mat absMat;

	Mat rgbMat;
	Mat spareBuffer;
	Mat fgmaskMat;
	Mat fgMat; // webcam image on foreground object
	Mat bgMat;
	// for finding the center of fgmaskMat blob

	Texture2D exampleTex;

	double SumXWeight, SumYWeight, SumIntensity, sumXex, sumYex,sumIntensityEx;
	double AvgXPos, AvgYPos;

	BackgroundSubtractorMOG2 backgroundSubstractorMOG2;
	bool frameBufferInit = false;
	//testing to see changes
	int updateCount = 0;

	public void Init(Mat matIn,WebCamTexture wct){
		
		wCT = wct;
		wbHelper = gameObject.GetComponent<WebCamHelper> ();
		bgMat = matIn;

		//set 1/ n and 1-(1/n) for accumBuffer weights
		currentFrameWeight = 1 / (float)NumberOfFrames;
		accumFrameWeight = 1 - currentFrameWeight;


		//Debug.Log ("fram percent = "+ framePercent);
		colorArraySize = matIn.height();
		//Debug.Log ("BUFFER INIT " + colorArraySize );

		currentMat = new Mat (matIn.rows (), matIn.cols (), CvType.CV_8UC1);
		accumBuffer = new Mat (matIn.rows (), matIn.cols (), CvType.CV_8UC1);
		absMat = new Mat (matIn.rows (), matIn.cols (), CvType.CV_8UC1);

		fgMat = new Mat (matIn.rows (), matIn.cols (), CvType.CV_8UC4);
		//bgMat = new Mat (matIn.rows (), matIn.cols (), CvType.CV_8UC4);
		rgbMat = new Mat (matIn.rows (), matIn.cols (), CvType.CV_8UC3);
		fgmaskMat = new Mat (matIn.rows (), matIn.cols (), CvType.CV_8UC1);

		backgroundSubstractorMOG2 = Video.createBackgroundSubtractorMOG2 (1000,25,false);

	
		frameBufferInit = true;
	}


	// Update is called once per frame
	void Update () {
		
		if(frameBufferInit){
//			Mat rgbaMat = webCamTextureToMatHelper.GetMat ();
//
//			Imgproc.cvtColor (rgbaMat, rgbMat, Imgproc.COLOR_RGBA2RGB);
//			backgroundSubstractorMOG2.apply (rgbMat, fgmaskMat);
//
//			Core.bitwise_not (fgmaskMat, fgmaskMat);
//			rgbaMat.setTo (new Scalar (0, 0, 0, 0), fgmaskMat);
//
//			Utils.matToTexture2D (rgbaMat, texture, colors);

			bgMat = wbHelper.GetMat();
			// for mean
			//			Imgproc.cvtColor (bgMat, currentMat, Imgproc.COLOR_RGBA2GRAY);
			//			Core.absdiff (currentMat,accumBuffer,absMat);
			//			Imgproc.threshold(absMat,fgmaskMat,accumThreshold,255,Imgproc.THRESH_BINARY);
			//^^ for mean

			Imgproc.cvtColor (bgMat, rgbMat, Imgproc.COLOR_RGBA2RGB);
			bgMat.copyTo (fgMat);
			backgroundSubstractorMOG2.apply (rgbMat, fgmaskMat);

//			// Read image
//			Mat im = imread( "blob.jpg", IMREAD_GRAYSCALE );
//
//			// Set up the detector with default parameters.
//			SimpleBlobDetector detector;
//
//			// Detect blobs.
//			std::vector<KeyPoint> keypoints;
//			detector.detect( im, keypoints);
//
//			// Draw detected blobs as red circles.
//			// DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
//			Mat im_with_keypoints;
//			drawKeypoints( im, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
//
//			// Show blobs
//			imshow("keypoints", im_with_keypoints );
//			waitKey(0);



		//	Core.bitwise_not (fgmaskMat, fgmaskMat);
			fgMat.setTo (new Scalar (0, 0, 0, 0), fgmaskMat);

		//	fgmaskMat.setTo (new Scalar (0, 0, 0, 0), fgmaskMat);
//			currentMat.setTo (rgbMat, fgmaskMat);
			//Core.subtract (accumBuffer,currentMat,spareBuffer);
		
			Core.addWeighted (currentMat,currentFrameWeight , accumBuffer, accumFrameWeight, 0, accumBuffer);

		}
	}

	public Mat getAccumBuffer(){
		return accumBuffer;
	}

	public Mat getSpareBuffer(){
		return spareBuffer;
	}

	public Mat getCurrentMat(){
		return currentMat;
	}

	public Mat getFgMat(){
		return fgMat;
	}
	public Mat getFgMaskMat(){
		return fgmaskMat;
	}

	public Mat getBgMat(){
		return bgMat;
	}
//	public int getXAvg(){
//		return (int)AvgXPos;
//	}
//
//	public int getYAvg(){
//		return (int)AvgYPos;
//	}
//
//	public Mat getXWeight(){
//		return Xweight;
//	}
//
//	public Mat getExample (){
//		return exampleMat;
//	}
//
//	public Mat getXpose (){
//		return Xposition;
//	}
	public static void SaveTextureToFile(Texture2D texture, string path)
	{
		byte[] pngBytes = texture.EncodeToPNG();
		System.IO.File.WriteAllBytes(path, pngBytes);
		AssetDatabase.Refresh();
	}
	public void Circle(Texture2D tex, int cx, int cy, int r, Color col)
	{
		int x, y, px, nx, py, ny, d;

		for (x = 0; x <= r; x++)
		{
			d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
			for (y = 0; y <= d; y++)
			{
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;

				tex.SetPixel(px, py, col);
				tex.SetPixel(nx, py, col);

				tex.SetPixel(px, ny, col);
				tex.SetPixel(nx, ny, col);

			}
		}    
	}
}
